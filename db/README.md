# Database Setup

See example instructions for ubuntu [here](https://kitcharoenp.github.io/postgresql/postgis/2018/05/28/set_up_postgreSQL_postgis.html)

# Database Migrations

Run the SQL file in this directory, this file assumes you have already setup
PostGIS and have created a "tac_scribe" user. Modify the SQL if this is not
the case.

