CREATE TABLE public.units
(
    id text COLLATE pg_catalog."default" NOT NULL,
    "position" geography NOT NULL,
    altitude double precision DEFAULT 0,
    type text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    pilot text COLLATE pg_catalog."default",
    "group" text COLLATE pg_catalog."default",
    coalition integer,
    heading integer,
    updated_at timestamp without time zone,
    CONSTRAINT units_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.units
    OWNER to tac_scribe;
