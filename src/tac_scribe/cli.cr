require "option_parser"
require "./daemon"

options = {
  "tacview_host"        => "localhost",
  "tacview_port"        => 42_674,
  "tacview_password"    => "",
  "tacview_client_name" => "TacScribe",
  "db_host"             => "localhost",
  "db_port"             => 5432,
  "db_name"             => "tac_scribe",
  "db_username"         => "tac_scribe",
  "db_password"         => "tac_scribe",
  "populate_airfields"  => false,
  "whitelist"           => false,
}

OptionParser.parse do |parser|
  parser.banner = "Usage: ruby start_scribe [options]"

  parser.separator "\nTacview Options"
  parser.on("--tacview-host=host",
    "Tacview server hostname / IP (Default: localhost)") do |v|
    options["tacview_host"] = v
  end

  parser.on("--tacview-port=port",
    "Tacview server port (Default: 42674)") do |v|
    options["tacview_port"] = v
  end

  parser.on("--tacview-password=password",
    "Tacview server password (Optional)") do |v|
    options["tacview_password"] = v
  end

  parser.on("--tacview-client-name=client",
    "Client name (Default: TacScribe)") do |v|
    options["tacview_client_name"] = v
  end

  parser.separator "\nDatabase Options"
  parser.on("--db-host=host",
    "Postgresql server hostname / IP (Default: localhost)") do |v|
    options["db_host"] = v
  end
  parser.on("--db-port=port",
    "Postgresql server port (Default: 5432)") do |v|
    options["db_port"] = v
  end
  parser.on("--db-name=name",
    "Postgresql database name (Default: tac_scribe)") do |v|
    options["db_name"] = v
  end
  parser.on("--db-username=username",
    "Postgresql username (Default: tac_scribe)") do |v|
    options["db_username"] = v
  end
  parser.on("--db-password=password",
    "Postgresql password (Default: tac_scribe") do |v|
    options["db_password"] = v
  end
  parser.separator "\nMisc options"
  parser.on("--populate-airfields",
    "Populate DCS airfield locations") do
    options["populate_airfields"] = true
  end
  parser.on("--verbose",
    "Verbose logging") do
    options["verbose"] = true
  end
  parser.on("--whitelist=whitelist",
    "Whitelist filename") do |v|
    options["whitelist"] = v
  end

  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end

  parser.missing_option do |option|
    puts "#{option} value required"
    exit(1)
  end
end

daemon = TacScribe::Daemon.new(tacview_host: options["tacview_host"].as(String),
  tacview_port: options["tacview_port"].as(Int32),
  tacview_client_name: options["tacview_client_name"].as(String),
  tacview_password: options["tacview_password"].as(String),
  db_host: options["db_host"].as(String),
  db_port: options["db_port"].as(Int32),
  db_name: options["db_name"].as(String),
  db_username: options["db_username"].as(String),
  db_password: options["db_password"].as(String),
  whitelist: options["whitelist"].as(String)
)
daemon.start_processing
