require "tacview_client"
require "./event_queue"

module TacScribe
  class EventEnqueuer < TacviewClient::BaseProcessor
    def initialize(@event_channel : EventChannel)
      @reference_time = @time = Time.utc
    end

    def update_object(event)
      @event_channel.send(
        UnitUpdateEvent.new(object: event, time: @time)
      )
    end

    def delete_object(object_id)
      @event_channel.send(
        UnitDeletionEvent.new(object_id: object_id)
      )
    end

    def update_time(time)
      millis = (time * 1000).to_i
      @time = @reference_time + Time::Span.new(0, 0, 0, 0, millis)
    end

    def set_property(property, value)
      case property
      when "ReferenceLatitude"
        @event_channel.send ReferenceLatitudeSettingEvent.new(value: BigDecimal.new(value))
      when "ReferenceLongitude"
        @event_channel.send ReferenceLongitudeSettingEvent.new(value: BigDecimal.new(value))
      when "ReferenceTime"
        @reference_time = @time = Time::Format::ISO_8601_DATE_TIME.parse(value)
      end
    end
  end
end
