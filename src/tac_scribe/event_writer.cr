require "./event_queue"

module TacScribe
  class EventWriter
    property reference_latitude : BigDecimal
    property reference_longitude : BigDecimal

    def initialize(@event_channel : EventChannel, @datastore : TacScribe::Datastore)
      @reference_latitude = BigDecimal.new(0)
      @reference_longitude = BigDecimal.new(0)
      @cache = Hash(String, Hash(String, String | BigDecimal)).new
      @update_times = Hash(String, Time).new
    end

    def start
      spawn do
        puts "#{Time.utc} EventWriter: Spawned"
        loop do
          process_event(@event_channel.receive)
        end
      rescue Channel::ClosedError
        puts "#{Time.utc} EventWriter: Channel Closed"
      end
    end

    private def process_event(event)
      case event
      when UnitUpdateEvent
        update_object(event.object, event.time)
      when UnitDeletionEvent
        delete_object(event.object_id)
      when ReferenceLatitudeSettingEvent
        @reference_latitude = event.value
      when ReferenceLongitudeSettingEvent
        @reference_longitude = event.value
      end
    end

    private def update_object(object : Hash(String, String | BigDecimal), time : Time)
      if @reference_latitude != 0 || @reference_longitude != 0
        localize_position(object)
      end

      id = object["object_id"].as(String)

      cache_object = @cache[id]?
      if cache_object
        object["heading"] = calculate_heading(cache_object, object)
        cache_object.merge!(object)
      else
        object["heading"] = BigDecimal.new(-1)
        @update_times[id] = Time.utc
        cache_object = object
      end
      @cache[id] = cache_object

      unless (Time.utc - @update_times[id]).total_seconds < 1
        @datastore.upsert_object(cache_object)
        @update_times[id] = Time.utc
      end
    end

    private def delete_object(object_id)
      @cache.delete(object_id)
      @datastore.delete_object(object_id)
    end

    private def localize_position(object)
      if object.has_key?("latitude")
        object["latitude"] = @reference_latitude + object["latitude"].as(BigDecimal)
      end
      if object.has_key?("longitude")
        object["longitude"] = @reference_longitude + object["longitude"].as(BigDecimal)
      end
    end

    private def calculate_heading(cached_object, new_object) : BigDecimal
      return BigDecimal.new(0) unless cached_object.has_key?("longitude") &&
                                      cached_object.has_key?("latitude") &&
                                      new_object.has_key?("longitude") &&
                                      new_object.has_key?("latitude")

      base_long = cached_object["longitude"].as(BigDecimal)
      base_lat = cached_object["latitude"].as(BigDecimal)
      new_long = new_object["longitude"].as(BigDecimal)
      new_lat = new_object["latitude"].as(BigDecimal)

      return BigDecimal.new(0) if new_long == base_long && new_lat == new_lat
      a, b = (new_long - base_long).to_f, (new_lat - base_lat).to_f
      res = Math.acos(b / Math.sqrt(a * a + b * b)) / Math::PI * 180
      a < 0 ? BigDecimal.new(360 - res) : BigDecimal.new(res)
    end
  end
end
