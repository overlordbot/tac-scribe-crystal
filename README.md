# TacScribe

Writes object state from a tacview server to a PostGIS extended Postgresql
database for further processing by other systems.

## Installation

TODO: Write installation instructions here

## Usage

You can run the tool using the `tac_scribe` command. Use the `--help`
option for information on required arguments

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/tac_scribe/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Jeffrey Jones](https://github.com/your-github-user) - creator and maintainer
